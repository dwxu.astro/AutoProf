astropy==3.2.3
astroquery==0.3.10
matplotlib==3.1.2
numpy==1.17.4
photutils==0.7.2
scikit-learn==0.21.3
scipy==1.3.3
